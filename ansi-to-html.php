#!/usr/bin/env php
<?php

function i($f)
{
    $f = $f . '/vendor/autoload.php';
    if (file_exists($f)) {return require $f;}
}

i(dirname(dirname(dirname(__DIR__)))) ||
i(__DIR__) ||
die(1);
# See phpunit/phpunit/phpunit for more inspiration!

use SensioLabs\AnsiConverter\AnsiToHtmlConverter;

$converter = new AnsiToHtmlConverter();

echo <<<'EOS'
<html>
    <body>
        <pre style="background-color: black; overflow: auto; padding: 10px 15px; font-family: monospace; font-weight: bold;"
        >
EOS;
echo $converter->convert(stream_get_contents(STDIN));
echo <<<'EOS'
        </pre>
    </body>
</html>

EOS;

# file_put_contents('php://output', $converter->convert(stream_get_contents(STDIN))); # works also

?>
