# ansi-to-html-cli

CLI for https://github.com/sensiolabs/ansi-to-html

* https://packages.debian.org/en/colorized-logs
* https://github.com/seebi/dircolors-solarized

# Old name in Packagist
* [php-util/ansi-to-html-cli
  ](https://packagist.org/packages/php-util/ansi-to-html-cli)
